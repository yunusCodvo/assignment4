import React, { useState } from 'react';

import ExpenseForm from './ExpenseForm';
import './NewExpense.css';

const NewExpense = ({ onSaveExpense }) => {
  const [showForm, setShowForm] = useState(false);

  const onAddExpense = (expense) => {
    const objExp = {
      ...expense,
      id: Math.random().toString(),
    };
    onSaveExpense(objExp);
    setShowForm(false);
  };
  const handleCloseForm = () => {
    setShowForm(false);
  };
  const handleOpenForm = () => {
    setShowForm(true);
  };
  return (
    <div className="new-expense">
      {!showForm && <button onClick={handleOpenForm}>Add New Expense</button>}
      {showForm && (
        <ExpenseForm
          onAddExpense={onAddExpense}
          onCloseForm={handleCloseForm}
        />
      )}
    </div>
  );
};

export default NewExpense;
