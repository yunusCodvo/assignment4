import React, { useState } from 'react';
import '../../App.css';
import Card from '../UI/Card';
import ExpensesFilter from './ExpensesFilter';
import ExpensesList from './ExpensesList';
const Expense = ({ expense }) => {
  const [filteredYear, setFilteredYear] = useState('2020');

  //Filter dropdown function
  const filterChangeHandler = (selectedYear) => {
    setFilteredYear(selectedYear);
  };
  const filteredExpenses = expense?.filter(
    (el) => el.date.getFullYear() == filteredYear
  );
  console.log('filteredExpenses ', filteredExpenses);
  return (
    <Card className="expenses">
      <ExpensesFilter
        selected={filteredYear}
        onChangeFilter={filterChangeHandler}
      />
      <ExpensesList filteredExpenses={filteredExpenses} />
    </Card>
  );
};

export default Expense;
