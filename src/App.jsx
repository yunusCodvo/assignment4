import React, { useState } from 'react';
import Expense from './components/Expenses/Expense';
import NewExpense from './components/NewExpense/NewExpense';
const INITIAL_EXPENSE = [
  {
    id: 'a1',
    title: 'Laptop',
    amount: 94.12,
    date: new Date(2020, 7, 14),
  },
  { id: 'b2', title: 'New TV', amount: 799.49, date: new Date(2021, 2, 12) },
  {
    id: 'c3',
    title: 'Mobile',
    amount: 294.67,
    date: new Date(2020, 2, 28),
  },
  {
    id: 'd4',
    title: 'New Desk (Wooden)',
    amount: 450,
    date: new Date(2022, 5, 12),
  },
];
function App() {
  //Initialize expense state with dummy data
  const [expenses, setExpenses] = useState(INITIAL_EXPENSE);

  //Add expense function
  const onAddExpense = (expense) => {
    setExpenses((prevState) => [expense, ...prevState]);
  };
  // console.log('exp ', expenses);
  return (
    <div>
      <h2 style={{ textAlign: 'center', color: '#fff' }}>Expense List</h2>
      <NewExpense onSaveExpense={onAddExpense} />
      <Expense expense={expenses} />
    </div>
  );
}

export default App;
